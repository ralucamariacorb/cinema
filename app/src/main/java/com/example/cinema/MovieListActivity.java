package com.example.cinema;

import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class MovieListActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    List<Movie> movies;
    private ListView listViewMovies;
    private static MovieAdapter customMovieAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        listViewMovies = findViewById(R.id.movieListView);
        databaseHelper = new DatabaseHelper();

        try {
            listAvailableMovies();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    /**
     * Gets all the movies from the database
     */
    private void getAllMoviesFromDatabase(){
        try {
            movies = databaseHelper.findAllMovies();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the adapter to show the available movies in the cinema
     * @throws ExecutionException
     * @throws InterruptedException
     * @throws ParseException
     */
    private void listAvailableMovies() throws ExecutionException, InterruptedException, ParseException {
        getAllMoviesFromDatabase();
        List<Movie> availableMovies = getCurrentDateMovies();
        ArrayList<Movie> movieArrayList = new ArrayList<>(availableMovies);
        customMovieAdapter = new MovieAdapter(movieArrayList, getApplicationContext());
        listViewMovies.setAdapter(customMovieAdapter);
    }

    /**
     * Get the cinema date for the movie and parse it
     * @return all the movies which are available to watch
     * @throws ParseException
     */
    private List<Movie> getCurrentDateMovies() throws ParseException {
        List<Movie> currentMovies = new ArrayList<>();
        List<Date> dates = new ArrayList<>();
        for (Movie movie: movies) {
            String cinemaDateString = movie.getCinemaDate();
            String datePattern = "yyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);

            Date cinemaDate = simpleDateFormat.parse(cinemaDateString);

            if(cinemaDate.after(new Date())){
                currentMovies.add(movie);
            }
        }
//        currentMovies = sortMoviesByCinemaDate(currentMovies);
        return currentMovies;
    }

//    private List<Movie> sortMoviesByCinemaDate(List<Movie> movies){
////        Collections.sort(movies, new SortDate());
//    }

    static class SortDate implements Comparator<Date> {

        @Override
        public int compare(Date date, Date t1) {
            return date.compareTo(t1);
        }
    }
}