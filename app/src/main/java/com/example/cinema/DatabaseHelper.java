package com.example.cinema;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DatabaseHelper {
    String SERVER_BASE_URL = "http://192.168.1.9/cinemaPDM/public/";
    String GET_USER = "user/get/";
    String ADD_USER = "user/add/";

    String GET_MOVIES = "movie/get";
    String GET_MOVIE = "movie/get/";

    /***
     * Find user by email and password
     * @param email
     * @param pass
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public User findUserByEmailAndPass(String email, String pass) throws ExecutionException, InterruptedException {
        String result = new RestClient(SERVER_BASE_URL + GET_USER + email + "/" + pass).execute().get();
        Gson gson = new Gson();
        return gson.fromJson(result, User.class);
    }

    /**
     * Insert user into database
     * @param user
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public boolean insertUser(User user) throws ExecutionException, InterruptedException {
        Gson gson = new Gson();
        String serializedUser = gson.toJson(user);
        String result = new RestClient(SERVER_BASE_URL + ADD_USER + serializedUser).execute().get();
        User userFromPHP = gson.fromJson(result, User.class);
        return userFromPHP != null;
    }

    /**
     * Return all movies
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public List<Movie> findAllMovies() throws ExecutionException, InterruptedException {
        String result = new RestClient(SERVER_BASE_URL + GET_MOVIES).execute().get();
        Gson gson = new Gson();
        Movie[] movies = gson.fromJson(result, Movie[].class);
        return Arrays.asList(movies);
    }

    public Movie findMovieById(int id) throws ExecutionException, InterruptedException {
        String result = new RestClient(SERVER_BASE_URL + GET_MOVIE + id ).execute().get();
        Gson gson = new Gson();
        return gson.fromJson(result, Movie.class);
    }
}
