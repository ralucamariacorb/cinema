package com.example.cinema;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class UserAdapter extends ArrayAdapter<User> {
    private final List<User> users;
    private final Context context;

    //constructor
    public UserAdapter(Context context, int resource, List<User> users) {
        super(context, resource, users);
        this.context = context;
        this.users = users;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //getting the layoutinflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //getting listview itmes
        View listViewItem = inflater.inflate(R.layout.users, null, true);
        TextView textViewName = listViewItem.findViewById(R.id.textViewName);
        TextView textViewPhone = listViewItem.findViewById(R.id.textViewPhone);
        TextView textViewEmail = listViewItem.findViewById(R.id.textViewEmail);
        TextView textViewPoints = listViewItem.findViewById(R.id.textViewPoints);


        //getting the current name
        User user = users.get(position);

        //setting the name to textview
        textViewName.setText("Name: " + user.firstname + " " + user.lastname);
        textViewPhone.setText("Phone: " + user.phone);
        textViewEmail.setText("Email: " + user.email);
        textViewPoints.setText("Points: "+ user.points);

        return listViewItem;
    }
}
