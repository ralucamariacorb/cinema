package com.example.cinema;

public class Seat {
    public int id_seat;
    public int row;
    public int col;
    public boolean occupied;
    public Room room;

    public Seat(int row, int col, boolean occupied, Room room) {
        this.row = row;
        this.col = col;
        this.occupied = occupied;
        this.room = room;
    }

    @Override
    public String toString() {
        return "Seat{" +
                "id_seat=" + id_seat +
                ", row='" + row + '\'' +
                ", col='" + col + '\'' +
                ", occupied='" + occupied + '\'' +
                ", room='" + room +
                '}';
    }
}
