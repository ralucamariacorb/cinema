package com.example.cinema;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutionException;

public class CreateAccountActivity extends AppCompatActivity {
    DatabaseHelper myDb;
    private EditText firstname;
    private EditText lastname;
    private EditText phone;
    private EditText email;
    private EditText pass;
    private Button createAccountButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_account);

        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.pass);
        createAccountButton = findViewById(R.id.createAccountButton);

        myDb = new DatabaseHelper();
        CreateAccount();
    }

    public void CreateAccount(){
        createAccountButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        User user = new User(firstname.getText().toString(), lastname.getText().toString(), phone.getText().toString(), email.getText().toString(), pass.getText().toString(), 0);
                        boolean isInserted = false;
                        try {
                            isInserted = myDb.insertUser(user);
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if(isInserted){
                            Toast.makeText(CreateAccountActivity.this, "Account created!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(CreateAccountActivity.this, MovieListActivity.class));
                        }
                        else{
                            Toast.makeText(CreateAccountActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }

}
