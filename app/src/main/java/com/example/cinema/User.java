package com.example.cinema;

public class User {
    public int id_user;
    public String firstname;
    public String lastname;
    public String phone;
    public String email;
    public String password;
    public int points;

    public User(String firstname, String lastname, String phone, String email, String password, int points) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.points = points;
    }

    @Override
    public String toString() {
        return "User{" +
                "id_user=" + id_user +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", points=" + points +
                '}';
    }
}
