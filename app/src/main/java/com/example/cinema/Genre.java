package com.example.cinema;

public class Genre {
    public int id_genre;
    public String name;


    public Genre(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id_genre=" + id_genre +
                ", name='" + name +
                '}';
    }
}
