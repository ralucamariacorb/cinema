package com.example.cinema;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.concurrent.ExecutionException;

public class LoginActivity extends AppCompatActivity {
    DatabaseHelper myDb;
    private EditText email;
    private EditText pass;
    private Button loginButton;
    private Button createAccountButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        email = findViewById(R.id.email);
        pass = findViewById(R.id.pass);
        loginButton = findViewById(R.id.loginButton);
        createAccountButton = findViewById(R.id.createAccountButton);

        myDb = new DatabaseHelper();
        Login();
        CreateAccount();
    }

    public void Login() {
        loginButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        User user = null;
                        try {
                            user = myDb.findUserByEmailAndPass(email.getText().toString(), pass.getText().toString());
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (user != null) {
                            Toast.makeText(LoginActivity.this, "Account exists!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(LoginActivity.this, MovieListActivity.class));
                        } else {
                            Toast.makeText(LoginActivity.this, "Error!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }

    public void CreateAccount() {
        createAccountButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    startActivity(new Intent(LoginActivity.this, CreateAccountActivity.class));
                    }
                }
        );
    }
}