package com.example.cinema;

import java.util.Date;

public class Program {
    public int id_program;
    public Date date;
    public int hour;
    public int minutes;

    public Program(Date date, int hour, int minutes) {
        this.date = date;
        this.hour = hour;
        this.minutes = minutes;
    }

    @Override
    public String toString() {
        return "Program{" +
                "id_program=" + id_program +
                ", date='" + date + '\'' +
                ", hour='" + hour + '\'' +
                ", minutes='" + minutes +
                '}';
    }
}
