package com.example.cinema;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RestBasedClientActivity extends Activity {
    private ListView listViewUsers;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rest_based_client);
        listViewUsers = findViewById(R.id.listViewUsers);
        String result = "String-ul primit de la server";
        Gson gson = new Gson();
        User[] usersFromPHP = gson.fromJson(result, User[].class);
        loadUsers(usersFromPHP);
        System.out.println(result);
    }

    /*
     * this method will
     * load the names from the database
     * with updated sync status
     * */
    private void loadUsers(User[] usersFromPHP) {
        //adapterobject for list view
        List<User> usersList = new ArrayList<>(Arrays.asList(usersFromPHP));
        UserAdapter userAdapter = new UserAdapter(this, R.layout.users, usersList);
        listViewUsers.setAdapter(userAdapter);
    }


}
