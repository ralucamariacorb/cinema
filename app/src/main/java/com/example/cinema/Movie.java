package com.example.cinema;

import java.util.List;

public class Movie {
    private int id;
    private String title;
    private String premierDate;
    private String cinemaDate;
    private String rating;
    private String directors;
    private String actors;
    private String description;
    private Integer runtime;
    private String type;
    private Double imdbGrade;
    private String trailer;
    private List<Genre> genres;
    private List<Program> programs;
    private String picture;

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", premierDate='" + premierDate + '\'' +
                ", cinemaDate='" + cinemaDate + '\'' +
                ", rating=" + rating +
                ", directors='" + directors + '\'' +
                ", actors='" + actors + '\'' +
                ", description='" + description + '\'' +
                ", runtime=" + runtime +
                ", type='" + type + '\'' +
                ", imdbGrade=" + imdbGrade +
                ", trailer='" + trailer + '\'' +
                ", genres=" + genres +
                ", programs=" + programs +
                ", picture='" + picture + '\'' +
                '}';
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Program> getPrograms() {
        return programs;
    }

    public void setPrograms(List<Program> programs) {
        this.programs = programs;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPremierDate() {
        return premierDate;
    }

    public void setPremierDate(String premierDate) {
        this.premierDate = premierDate;
    }

    public String getCinemaDate() {
        return cinemaDate;
    }

    public void setCinemaDate(String cinemaDate) {
        this.cinemaDate = cinemaDate;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getImdbGrade() {
        return imdbGrade;
    }

    public void setImdbGrade(Double imdbGrade) {
        this.imdbGrade = imdbGrade;
    }
}
