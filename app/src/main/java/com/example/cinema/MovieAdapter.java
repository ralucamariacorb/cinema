package com.example.cinema;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Base64;

import java.util.ArrayList;

public class MovieAdapter extends ArrayAdapter<Movie> {

    private final ArrayList<Movie> movies;
    private final Context context;
    private int lastPosition = -1;
    private String movieDetailButtonTest = "View details";

    private static class ViewHolder{
        TextView movieTitle;
        Button movieDetailsButton;
        ImageView movieImage;
        TextView filmType;
        TextView imdbGrade;
        TextView runtime;
    }

    public MovieAdapter(ArrayList<Movie> movies, Context context) {
        super(context, R.layout.movie_row, movies);
        this.movies = movies;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Movie movie = getItem(position);
        final View result;
        final ViewHolder viewHolder;

        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.movie_row, parent, false);
            viewHolder.movieTitle = convertView.findViewById(R.id.filmTitle);
            viewHolder.movieDetailsButton = convertView.findViewById(R.id.filmDetail);
            viewHolder.filmType = convertView.findViewById(R.id.filmType);
            viewHolder.imdbGrade = convertView.findViewById(R.id.imdbGrade);
            viewHolder.movieImage = convertView.findViewById(R.id.moviePicture);
            viewHolder.runtime = convertView.findViewById(R.id.movieRuntime);
            result=convertView;
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.movieTitle.setText(movie.getTitle());
        viewHolder.movieDetailsButton.setText(this.movieDetailButtonTest);
        viewHolder.filmType.setText(movie.getType());
        final String imdbGradeString = "IMDB " + String.valueOf(movie.getImdbGrade());
        viewHolder.imdbGrade.setText(imdbGradeString);

        String movieRuntime = movie.getRuntime() + " min";
        viewHolder.runtime.setText(movieRuntime);

        // decoding the image sent from the server as base64 into bitmap
        byte[] decodedString = Base64.getDecoder().decode(movie.getPicture());
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        viewHolder.movieImage.setImageBitmap(decodedByte);

        viewHolder.movieDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MovieDetailActivity.class);
                intent.putExtra("movieId", movie.getId());
                context.startActivity(intent);
            }
        });

        return convertView;
    }


}
