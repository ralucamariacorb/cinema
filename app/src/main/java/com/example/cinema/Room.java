package com.example.cinema;

public class Room {
    public int id_room;
    public int freeSeatsNumber;
    public int occupiedSeatsNumber;

    public Room(int freeSeatsNumber, int occupiedSeatsNumber) {
        this.freeSeatsNumber = freeSeatsNumber;
        this.occupiedSeatsNumber = occupiedSeatsNumber;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id_room=" + id_room +
                ", freeSeatsNumber='" + freeSeatsNumber + '\'' +
                ", occupiedSeatsNumber='" + occupiedSeatsNumber +
                '}';
    }
}
