package com.example.cinema;

public class Ticket {
    public int id_ticket;
    public String type;
    public int price;
    public Rezervation rezervation;
    public Seat seat;

    public Ticket(String type, int price, Rezervation rezervation, Seat seat) {
        this.type = type;
        this.price = price;
        this.rezervation = rezervation;
        this.seat = seat;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id_ticket=" + id_ticket +
                ", type='" + type + '\'' +
                ", price='" + price + '\'' +
                ", rezervation='" + rezervation.toString() + '\'' +
                ", seat='" + seat.toString() +
                '}';
    }
}
