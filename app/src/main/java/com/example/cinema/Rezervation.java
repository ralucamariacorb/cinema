package com.example.cinema;

public class Rezervation {
    public int id_rezervation;
    public User user;
    public Program program;

    public Rezervation(User user, Program program) {
        this.user = user;
        this.program = program;
    }

    @Override
    public String toString() {
        return "Rezervation{" +
                "id_rezervation=" + id_rezervation +
                ", user='" + user.toString() + '\'' +
                ", program='" + program.toString() + '\'' +
                '}';
    }
}
