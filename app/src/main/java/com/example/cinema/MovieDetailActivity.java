package com.example.cinema;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import org.w3c.dom.Text;

import java.util.concurrent.ExecutionException;

public class MovieDetailActivity  extends AppCompatActivity {
    private TextView title;
    private TextView date;
    private TextView rate;
    private TextView cast;
    private YouTubePlayerView youTubePlayerView;
    DatabaseHelper myDb;
    private int movieIdFromMovieList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_detail);
        title = findViewById(R.id.titlid);
        date = findViewById(R.id.dateid);
        rate = findViewById(R.id.ratingid);
        cast = findViewById(R.id.castid);
        youTubePlayerView = findViewById(R.id.youtubePlayerView);
        myDb = new DatabaseHelper();
        movieIdFromMovieList = getIntent().getIntExtra("movieId",0);

        SetMovieInfo(movieIdFromMovieList);
    }

    public void SetMovieInfo(int id){
        Movie movie = null;
        try {
            movie = myDb.findMovieById(id);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if (movie != null) {
            final String[] trailer=movie.getTrailer().split("/");
            getLifecycle().addObserver(youTubePlayerView);
            youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                    String videoId = trailer[3];
                    youTubePlayer.loadVideo(videoId, 0f);
                }
            });

            //set info about the movie from db
            title.setText("Film: " + movie.getTitle()+", type: "+movie.getType());
            date.setText("Premire date: " + movie.getPremierDate()+" Cinema date: "+movie.getCinemaDate());
            rate.setText("Runtime: " + movie.getRuntime()+" Rating: "+movie.getRating()+" Grade:"+movie.getImdbGrade());
            cast.setText("Directors: " + movie.getDirectors()+" Actors: "+movie.getActors());
        }
    }


}
